package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetLastTweets(t *testing.T) {
	statuses, err := getLastTweets()
	assert.IsType(t, nil, err)
	assert.NotEqual(t, 0, len(statuses))
}
