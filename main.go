package main

import (
	"errors"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

var (
	ErrorBackend = errors.New("Something went wrong")
)

func Handler() ([]Status, error) {
	statuses, err := getLastTweets()
	if err != nil {
		return nil, err
	}

	fmt.Println(statuses)

	writeTweets(statuses)
	return statuses, nil
}

func main() {
	lambda.Start(Handler)
}
