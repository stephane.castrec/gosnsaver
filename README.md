# GOSNSaver
Let's save interesting hashtags from Tweeter and Instagram to DynamoDB.
Because of the use of Lambdas, we do not use [stream](https://developer.twitter.com/en/docs/labs/sampled-stream/overview) but [search](https://developer.twitter.com/en/docs/tweets/search/overview/standard)

## Var env
* TW_API_KEY : API key to access twitter APIs
* TW_SEARCH : Search to do on Twitter
* TW_DB_TABLE : DynamoDB table to store data