package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

var (
	TW_API_KEY = os.Getenv("TW_API_KEY")
	TW_SEARCH  = os.Getenv("TW_SEARCH")
)

type TwitterSearchResponse struct {
	Statuses []Status `json:"Statuses"`
}

type TwitterUser struct {
	Id          int64  `json:"id"`
	Name        string `json:"name"`
	Alias       string `json:"screen_name"`
	Description string `json:"description"`
	ProfilImg   string `json:"profile_image_url_https"`
}

type Hashtag struct {
	Text string `json:"text"`
}

type TweetUrl struct {
	Url     string `json:"url"`
	FullUrl string `json:"expanded_url"`
}

type TweetUrls struct {
	Urls []TweetUrl `json:"urls"`
}

type UserMentions struct {
	Name string `json:"screen_name"`
}

type Entities struct {
	Hashtags     []Hashtag      `json:"hashtags"`
	Urls         TweetUrls      `json:"url"`
	UserMentions []UserMentions `json:"user_mentions"`
}

type Status struct {
	Text        string      `json:"text"`
	Cover       string      `json:"poster_path"`
	CreatedDate string      `json:"created_at"`
	Id          int64       `json:"id"`
	User        TwitterUser `json:"user"`
	Entities    Entities    `json:"entities"`
}

func getLastTweets() ([]Status, error) {
	url := "https://api.twitter.com/1.1/search/tweets.json"
	authorization := fmt.Sprintf("Bearer %s", TW_API_KEY)

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("authorization", authorization)

	if err != nil {
		log.Print(err)
		return []Status{}, ErrorBackend
	}

	q := req.URL.Query()
	q.Add("q", TW_SEARCH)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		log.Print(err)
		return []Status{}, ErrorBackend
	}
	defer resp.Body.Close()
	//log.Print("tweet body = ", resp.Body)

	var data TwitterSearchResponse
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		log.Print("Error decoding tweet body = ", err)
		return []Status{}, ErrorBackend
	}

	log.Print("received nb tweets = ", len(data.Statuses))
	return data.Statuses, nil
}
