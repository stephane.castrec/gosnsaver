package main

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var (
	TW_DB_TABLE = os.Getenv("TW_DB_TABLE")
)

const MAX_ITEMS_WRITE = 25

func mapHashtags(hashtags []Hashtag) []string {
	var hash []string
	for _, element := range hashtags {
		hash = append(hash, element.Text)
	}
	return hash
}

func mapTweet(status Status) map[string]*dynamodb.AttributeValue {
	mapped, err := dynamodbattribute.MarshalMap(status)
	if err != nil {
		log.Println("Error mapTweet", status, err)
		return nil
	}
	fmt.Print(mapped)
	return mapped
	/*mapped := map[string]*dynamodb.AttributeValue{
		"id": {
			S: aws.String(status.Id),
		},
		"source": {
			S: aws.String("TWITTER"),
		},
		"search": {
			S: aws.String(TW_SEARCH),
		},
		"date": {
			S: aws.String(status.CreatedDate),
		},
		"text": {
			S: aws.String(status.Text),
		},
		"userId": {
			S: aws.String(status.User.Id),
		},
	}
	if status.User.Alias != "" {
		mapped["userAlias"] = *dynamodb.AttributeValue{
			S: aws.String(status.User.Alias),
		}
	}

	if status.User.Name != "" {
		mapped["userAlias"] = *dynamodb.AttributeValue{
			S: aws.String(status.User.Alias),
		}
	}

	if status.User.ProfilImg != "" {
		mapped["userAlias"] = *dynamodb.AttributeValue{
			S: aws.String(status.User.Alias),
		}
	}

	if len(status.hashtags) > 0 {
		mapped["hashtags"] = *dynamodb.AttributeValue{
			S: aws.String(status.User.Alias),
		}
	}

	return mapped*/
}

// FIXME : If more than MAX ITEMS, not writted
func mapTweetsToWriteItemRequest(statuses []Status) dynamodb.BatchWriteItemInput {
	i := 0
	input := &dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{},
	}

	// prepare data by MAX ITEMS.
	var wr []*dynamodb.WriteRequest
	for i < MAX_ITEMS_WRITE && i < len(statuses) {
		fmt.Printf("%d ", i)
		mapped := mapTweet(statuses[i])
		if mapped != nil {
			wr = append(wr, &dynamodb.WriteRequest{
				PutRequest: &dynamodb.PutRequest{
					Item: mapped,
				},
			})
		}
		i += 1
	}
	input.RequestItems[TW_DB_TABLE] = wr[0:i]

	return *input
}

func writeTweets(statuses []Status) {
	var sess = session.Must(session.NewSession())
	svc := dynamodb.New(sess)

	input := mapTweetsToWriteItemRequest(statuses)

	// sending a request using the BatchWriteItemRequest method.
	result, err := svc.BatchWriteItem(&input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeProvisionedThroughputExceededException:
				fmt.Println(dynamodb.ErrCodeProvisionedThroughputExceededException, aerr.Error())
			case dynamodb.ErrCodeResourceNotFoundException:
				fmt.Println(dynamodb.ErrCodeResourceNotFoundException, aerr.Error())
			case dynamodb.ErrCodeItemCollectionSizeLimitExceededException:
				fmt.Println(dynamodb.ErrCodeItemCollectionSizeLimitExceededException, aerr.Error())
			case dynamodb.ErrCodeRequestLimitExceeded:
				fmt.Println(dynamodb.ErrCodeRequestLimitExceeded, aerr.Error())
			case dynamodb.ErrCodeInternalServerError:
				fmt.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return
	}
	fmt.Println(result)
}
