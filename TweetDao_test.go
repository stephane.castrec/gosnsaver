package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMapHashtags(t *testing.T) {
	hashtags := []Hashtag{
		{
			Text: "Hash1",
		},
		{
			Text: "Hash2",
		},
	}
	var hash []string = mapHashtags(hashtags)
	assert.Equal(t, 2, len(hash))
	assert.Equal(t, "Hash1", hash[0])
	assert.Equal(t, "Hash2", hash[1])
}
